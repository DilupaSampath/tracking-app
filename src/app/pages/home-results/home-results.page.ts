import { Component, OnInit } from '@angular/core';
import {
  NavController,
  AlertController,
  MenuController,
  ToastController,
  PopoverController,
  ModalController } from '@ionic/angular';

// Modals
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { SearchFilterPage } from '../../pages/modal/search-filter/search-filter.page';
import { ImagePage } from './../modal/image/image.page';
// Call notifications test by Popover and Custom Component.
import { NotificationsComponent } from './../../components/notifications/notifications.component';
import { MainServicesService } from '../main-services.service';
@Component({
  selector: 'app-home-results',
  templateUrl: './home-results.page.html',
  styleUrls: ['./home-results.page.scss']
})
export class HomeResultsPage implements OnInit{
  ngOnInit(): void {
  }
  currentTrain:any;
  watch:any;
  respValue:any;
  status:boolean=true;
  searchKey = '';
  themeCover = 'https://cdn.dribbble.com/users/1514097/screenshots/3216203/location_animation.gif';
  geoLocationArray=[];
  responseData : any; 
  languageSelected: any;
  trainSelected:any;
  trainS:any;
  trainData:any[];
  trainLon:any;
  trainLat:any;
  isWatching:boolean
  constructor(
    public navCtrl: NavController,
    public menuCtrl: MenuController,
    public popoverCtrl: PopoverController,
    public alertCtrl: AlertController,
    public modalCtrl: ModalController,
    public toastCtrl: ToastController,
    public _MainServicesService:MainServicesService,
    private geolocation: Geolocation,
    public toastController: ToastController
  ) {
    this.isWatching=false;
this.getAllTrains();
  }
  
  async presentToastWithOptions(message,color) {
    const toast = await this.toastController.create({
      message: message,
      position: 'top',
      color:color,
       duration: 2000
    });
    toast.present();
  }
  setLanguage(event) {
    let me=this;
    console.log(event.target.value);
    this.trainSelected=event.target.value;
    this._MainServicesService.setId(event.target.value);
  }
  ionViewWillEnter() {
    this.menuCtrl.enable(true);
  }
//   [{
//     "type": "Point",
//     "coordinates": [
//         80.02864122390747,
//         7.131958239203191
//     ]
// }]
  trackingStatus(status:number){
    if(this.trainSelected){
      if(status==0){
        this.presentConfirm('Do you need to track this train ?','No,Thanks','Yes,Please Proceed','start');
      
      }else if(status==1){
        this.themeCover="https://cdn.dribbble.com/users/599768/screenshots/3033137/map-icon-train-station.gif";
      }else if(status==2){
        this.themeCover="https://cdn.dribbble.com/users/599768/screenshots/3032027/map-icon-bar.gif";
      }else if(status==3){
        this.presentConfirm('Do you need to stop tracking ?','No,Thanks','Yes,Please Proceed','stop');
      }
    
    }else{
      this.presentToastWithOptions("Please Select a Train First!!!","danger");

    }
  }
  async  presentConfirm(message,cancelText,okTex,type) {
    let alert =await  this.alertCtrl.create({
      message: message,
      buttons: [
        {
          text: cancelText,
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: okTex,
          handler: () => {
            if(type=='start'){
              this.themeCover="https://cdn.dribbble.com/users/599768/screenshots/3033137/map-icon-train-station.gif";
              this.storeLocationData();
              this.presentToastWithOptions("Train Tracking Started","success");
            }else{
              this.themeCover="https://cdn.dribbble.com/users/330174/screenshots/2695600/comp_2.gif";
              this.watch.unsubscribe();
              this.isWatching = false;
              this._MainServicesService.postLocationData();
              this.presentToastWithOptions("Train Tracking Finished..!","success");
              setTimeout(() => { 
                this.themeCover='https://cdn.dribbble.com/users/1514097/screenshots/3216203/location_animation.gif';
              }, 6200);
            }
            console.log('Buy clicked');
          }
        }
      ]
    });
    alert.present();
  }
  getAllTrains(){
    this._MainServicesService.get(`train/getAll`).subscribe(data=>{
      console.log(data);
      this.trainData=data.data;
    });
  }
  settings() {
    this.navCtrl.navigateForward('settings');
  }

setId(){
  console.log(this.trainSelected);
  // this._MainServicesService.setId(id);
}

storeLocationData(){
  this.isWatching = true;
  let watch = this.geolocation.watchPosition();
  this.watch =watch
  .subscribe((resp) => {
    this.respValue=this._MainServicesService.cashedLocationPoints.length;
    if(resp.coords.longitude != undefined){
    this.respValue=this._MainServicesService.cashedLocationPoints.length;
    this.isWatching = true;
    this.respValue=this._MainServicesService.cashedLocationPoints.length;
    this.trainLon  = resp.coords.longitude;
    this.trainLat  = resp.coords.latitude;
    let lastLat =resp.coords.latitude;
    this._MainServicesService.storeGeoLocations(
      {
        "type": "Point",
        "coordinates": [
          resp.coords.longitude,
          resp.coords.latitude
        ]
    }
      );
  }
  
  });
  // for(let i=0;i<i+2;i++){
  //   if(!this.status){
  //     i=-100;
  //     this._MainServicesService.postLocationData();
  //   }else{
  //      let watch = this.geolocation.watchPosition();

  //     // this._MainServicesService.storeGeoLocations(currentLocation);
  //       }
  //   }
  
}
postLocationData(){
  this._MainServicesService.postLocationData();
  }

  async alertLocation() {
    const changeLocation = await this.alertCtrl.create({
      header: 'Tracking Settings',
      message: 'Current Tracking Time 2s',
      inputs: [
        {
          name: 'location',
          placeholder: 'Enter new tracking time',
          type: 'text'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Update',
          handler: async (data) => {
            console.log('Change clicked', data);
            this.currentTrain = data.location;
            const toast = await this.toastCtrl.create({
              message: 'Tracking time was change successfully',
              duration: 3000,
              position: 'top',
              closeButtonText: 'OK',
              showCloseButton: true
            });

            toast.present();
          }
        }
      ]
    });
    changeLocation.present();
  }

  async searchFilter () {
    const modal = await this.modalCtrl.create({
      component: SearchFilterPage
    });
    return await modal.present();
  }

  async presentImage(image: any) {
    const modal = await this.modalCtrl.create({
      component: ImagePage,
      componentProps: { value: image }
    });
    return await modal.present();
  }

  async notifications(ev: any) {
    const popover = await this.popoverCtrl.create({
      component: NotificationsComponent,
      event: ev,
      animated: true,
      showBackdrop: true
    });
    return await popover.present();
  }

}
