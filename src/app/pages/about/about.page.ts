import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { MainServicesService } from '../main-services.service';
import { NativeGeocoder } from '@ionic-native/native-geocoder/ngx';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { AlertController, ToastController } from '@ionic/angular';
declare var google;
@Component({
  selector: 'app-about',
  templateUrl: './about.page.html',
  styleUrls: ['./about.page.scss'],
})
export class AboutPage implements OnInit {
  trainData:any[];
  @ViewChild('map') mapElement: ElementRef;
  map: any;
  address:string;
  museums: [];
  constructor(      public toastController: ToastController,   public alertCtrl: AlertController,public _MainServicesService:MainServicesService, private geolocation: Geolocation,
    private nativeGeocoder: NativeGeocoder) { }

  ngOnInit() {
    this.loadMap();
    this.getAllTrains();
  }
  async presentToastWithOptions(message,color) {
    const toast = await this.toastController.create({
      message: message,
      position: 'top',
      color:color,
       duration: 2000
    });
    toast.present();
  }
  getMarkers(selectedPoints:any[],name:any) {
    
    // tslint:disable-next-line:variable-name
    for (let _i = 0; _i < selectedPoints.length; _i++) {
      if (_i > 0) {
        if(_i==1){
          this.addMarkersToMap(selectedPoints[_i].coordinates,name,true);
        }else{
          this.addMarkersToMap(selectedPoints[_i].coordinates,name,false);
        }

      }
    }
  }
  async  presentConfirm(message,cancelText,okTex,id) {
    let alert =await  this.alertCtrl.create({
      message: message,
      buttons: [
        {
          text: cancelText,
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: okTex,
          handler: () => {
            this._MainServicesService.remove(id);
            setTimeout(() => { 
              this.getAllTrains();
              this.presentToastWithOptions('Successfully Removed','success');
            }, 1500);  
            console.log('Buy clicked');
          }
        }
      ]
    });
    alert.present();
  }

removePath(id:any){
  this.presentConfirm('Do you need to remove this path?','No,Thanks','Yes,Please Proceed',id);
}
setMapNull(){
  this.map = this.map.setMap(null);
}
  addMarkersToMap(train,tite,status) {
    let museumMarker = new google.maps.Marker();
    if(status){
      const position = new google.maps.LatLng(train[1], train[0]);
       museumMarker = new google.maps.Marker({ position, title: tite,label:tite});
      
    }else{
      const position = new google.maps.LatLng(train[1], train[0]);
       museumMarker = new google.maps.Marker({ position});
      
    }
    museumMarker.setMap(this.map);

  }

  
  loadMap() {
    // this.geolocation.getCurrentPosition().then((resp) => {
    //   // resp.coords.latitude
    //   // resp.coords.longitude
    //   let mapOptions = {
    //     center: resp.coords,
    //     zoom: 15,
    //     mapTypeId: google.maps.MapTypeId.ROADMAP
    //   }
    //   alert(JSON.stringify( resp.coords));
    // this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
    // }).catch((error) => {
    //   alert('Can not retrieve Location')
    //    console.log('Error getting location', error);
    //  });


  
    let mapOptions = {
      center: new google.maps.LatLng(7.8731,80.7718),
      zoom: 7.4,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    }
  this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);


  }
  getAllTrains(){
    this._MainServicesService.get(`train/getAll`).subscribe(data=>{
      console.log(data);
      this.trainData=data.data;
    });
  }
}
export interface Museum{
  name: string;
  state : string;
  latitude: any;
  longitude:any
}